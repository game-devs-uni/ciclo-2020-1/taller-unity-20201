﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraManager : MonoBehaviour
{
    public GameObject[] cameras;
    public Dropdown ListCams;

    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject cam in cameras)
        {
            cam.SetActive(false);
        }

        cameras[0].SetActive(true);
    }

    public void SelectCamera()
    {
        foreach(GameObject cam in cameras)
        {
            cam.SetActive(false);
        }

        cameras[ListCams.value].SetActive(true);
    }
}

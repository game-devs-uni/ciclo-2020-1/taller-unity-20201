﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunGetterUI : MonoBehaviour
{
    public Image gunIcon;
    
    public void SetIcon(Sprite sprite)
    {
        gunIcon.sprite = sprite;
    }
}

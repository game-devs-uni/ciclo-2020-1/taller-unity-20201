﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GunSetterUI : MonoBehaviour
{
    private SpriteRenderer sprite;
    [SerializeField] private GameManager.Gun gun;
    
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();

        switch (gun)
        {
            case GameManager.Gun.HeavyMachineGun:
                sprite.sprite = GameManager.Instance.gunSprite[0];
                break;
            case GameManager.Gun.ShotGun:
                sprite.sprite = GameManager.Instance.gunSprite[1];
                break;
            case GameManager.Gun.FlameShot:
                sprite.sprite = GameManager.Instance.gunSprite[2];
                break;
            case GameManager.Gun.EnemyChaser:
                sprite.sprite = GameManager.Instance.gunSprite[3];
                break;
            case GameManager.Gun.LaserGun:
                sprite.sprite = GameManager.Instance.gunSprite[4];
                break;
            case GameManager.Gun.IronLizard:
                sprite.sprite = GameManager.Instance.gunSprite[5];
                break;
            case GameManager.Gun.DropShot:
                sprite.sprite = GameManager.Instance.gunSprite[6];
                break;
            case GameManager.Gun.SuperGrenade:
                sprite.sprite = GameManager.Instance.gunSprite[7];
                break;
            case GameManager.Gun.ZantetsuSword:
                sprite.sprite = GameManager.Instance.gunSprite[8];
                break;
            case GameManager.Gun.nothing:
                sprite.sprite = GameManager.Instance.gunSprite[9];
                break;

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.gunGetterUI.SetIcon(sprite.sprite);
            GameManager.Instance.gun = gun;
            Destroy(gameObject);
            Debug.Log("Coger arma");
        }
    }

    
}

﻿using UnityEditor;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public int HealthPlayer=100;
    public Gun gun;
    public Sprite[] gunSprite;
    [HideInInspector] public GunGetterUI gunGetterUI;

    public enum Gun
    {
        HeavyMachineGun, 
        ShotGun,
        FlameShot,
        EnemyChaser,
        LaserGun,
        IronLizard, 
        DropShot,
        SuperGrenade,
        ZantetsuSword,
        nothing
    }

    private void Awake()
    {
        gunGetterUI = FindObjectOfType<GunGetterUI>();
        if (Instance==null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        else Destroy(gameObject);
    }
   
    private void OnLevelWasLoaded(int level)
    {
        gunGetterUI = FindObjectOfType<GunGetterUI>();
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionManager : MonoBehaviour
{
    public GameObject body;
    void UntagBody() {
        body.SetActive(false);
    }
    void ActiveBody() {
        body.SetActive(true);
    }
}

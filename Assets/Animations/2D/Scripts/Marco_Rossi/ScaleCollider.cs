﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleCollider : MonoBehaviour
{
    public GameObject marco;
    private CapsuleCollider2D col;
    private void Start()
    {
        col = marco.GetComponent<CapsuleCollider2D>();
    }
    void ToNormal() 
    {
        col.offset = new Vector2(col.offset.x, -0.26f);
        col.size = new Vector2(col.size.x, 1.58f);
    }
    void ToDown()
    {
        col.offset = new Vector2(col.offset.x, -0.55f);
        col.size = new Vector2(col.size.x, 1f);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarcoController : MonoBehaviour
{
    public float marcoSpeed = 2f;
    public float DownSpeedModificator = 0.2f;
    private Rigidbody2D rb2d;
    public GameObject marcoBody;
    public GameObject marcoLegs;
    public CheckFloor checker;
    private Animator anim_marcoBody;
    private Animator anim_marcoLegs;
    public float jumpForce;
    private bool jump;
    private bool down;
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim_marcoBody = marcoBody.GetComponent<Animator>();
        anim_marcoLegs = marcoLegs.GetComponent<Animator>();
    }

    void Update()
    {
        
        jump = Input.GetKeyDown(KeyCode.LeftShift);
        down = Input.GetKey(KeyCode.DownArrow);
        if (jump && checker.is_grounded)
        {
            rb2d.AddForce(Vector3.up * jumpForce, ForceMode2D.Impulse);
        }


        UpdateAnimator();
    }

    private void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        //if (jump && checker.is_grounded) {
        //    rb2d.AddForce(new Vector3(0f, jumpForce, 0f), ForceMode2D.Impulse);
        //    jump = false;
        //}
        if(horizontal > 0.1f) {
            transform.localScale = new Vector3(1f, 1f, 1f);
        }
        if (horizontal < -0.1f) {
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        UpdateVelocity(horizontal);
    }


    void UpdateAnimator() {
        anim_marcoLegs.SetBool("is_grounded", checker.is_grounded);
        anim_marcoBody.SetBool("is_grounded", checker.is_grounded);
        anim_marcoBody.SetFloat("speed_x", Mathf.Abs(rb2d.velocity.x));
        anim_marcoLegs.SetFloat("speed_x", Mathf.Abs(rb2d.velocity.x));
        anim_marcoLegs.SetFloat("speed_y", rb2d.velocity.y);
        anim_marcoLegs.SetBool("down", down);
        anim_marcoBody.SetBool("down", down);
    }

    void UpdateVelocity(float horizontal) {
        if (down && checker.is_grounded)
        {
            rb2d.velocity = new Vector2(marcoSpeed * horizontal * DownSpeedModificator, rb2d.velocity.y);
        }
        else {
            rb2d.velocity = new Vector2(marcoSpeed * horizontal, rb2d.velocity.y);
        }
    }
}

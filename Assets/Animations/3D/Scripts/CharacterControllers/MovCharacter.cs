﻿    using UnityEngine;

public class MovCharacter : MonoBehaviour
{

    public GameObject pivotCam;
    public float velocity;

    private Rigidbody rbd;
    private Animator anim;
    
    private void Awake()
    {
        rbd = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        //MovAxis();
        Animations();

        //if (!active) rbd.isKinematic = true;
        //else rbd.isKinematic = false;
    }

    private void FixedUpdate()
    {
        MovAxis();
    }

    private void LateUpdate()
    {
        Rotation();
    }

    void Animations()
    {
        Vector3 velXZ = new Vector3(rbd.velocity.x,0,rbd.velocity.z);
        anim.SetFloat("vel", velXZ.magnitude);
    }

    void MovAxis()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 movDirZ = pivotCam.transform.forward;
        Vector3 movDirX = pivotCam.transform.right;
        
        Vector3 moveXZ= movDirZ * z + movDirX * x;
        rbd.velocity = Vector3.Slerp(rbd.velocity,new Vector3(0, rbd.velocity.y, 0) + moveXZ.normalized*velocity,Time.fixedDeltaTime*10);

        
    }

    void Rotation()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        transform.forward = Vector3.Slerp(transform.forward,pivotCam.transform.forward * z + pivotCam.transform.right * x,Time.deltaTime*10);
        
        //if (x == 1 || z == 1) transform.forward = pivotCam.transform.forward * z + pivotCam.transform.right * x;
    }
}

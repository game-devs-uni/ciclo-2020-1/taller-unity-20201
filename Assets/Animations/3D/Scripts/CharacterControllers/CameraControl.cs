﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public GameObject player;
    public GameObject pivotVertical;
    public GameObject thirdCamera;
    public float sensibility;
    public float time = 0.5f;
    public float mVertical;
    public float mHorizontal;
    // Start is called before the first frame update
    void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;
        //Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        CameraMove();
        
    }

    private void FixedUpdate()
    {
        PivotPosition();
    }

    void PivotPosition()
    {
        Vector3 target= player.transform.position;
        Vector3 vel =new  Vector3();
        transform.position = Vector3.SmoothDamp(transform.position,target,ref vel,time,10,Time.fixedDeltaTime);
        //transform.position = Vector3.Lerp(transform.position, target, Time.fixedDeltaTime * 10);
    }

    void CameraMove()
    {
        thirdCamera.transform.LookAt(transform.localPosition);

        float x = Input.GetAxis("Mouse X");
        float z = Input.GetAxis("Mouse Y");

        mVertical += -z * sensibility * Time.deltaTime;
        mHorizontal += x * sensibility * Time.deltaTime;

        mVertical = Mathf.Clamp(mVertical, -50, 50);

        transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0, mHorizontal, 0), Time.deltaTime*10);
        pivotVertical.transform.localRotation = Quaternion.Slerp(pivotVertical.transform.localRotation,Quaternion.Euler(mVertical, 0, 0), Time.deltaTime * 10);  
    }
}

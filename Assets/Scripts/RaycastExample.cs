﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastExample : MonoBehaviour
{
    Outline currentOutline;
    LineRenderer m_lr = null;
    public float maxDistance = 100;

    private void Start()
    {
        if(GetComponent<LineRenderer>()) m_lr = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, transform.forward, out hit, maxDistance))
        {
            Outline newOutline = hit.collider.GetComponent<Outline>();
            if (newOutline != currentOutline)
            {
                if (currentOutline) currentOutline.enabled = false;
                currentOutline = newOutline;
                Debug.Log("Rasho laser hits: " + hit.collider.name);
                if (currentOutline) currentOutline.enabled = true;
            }

            m_lr.SetPosition(0, transform.position);
            m_lr.SetPosition(1, hit.point);
        }
        else
        {
            if (currentOutline) currentOutline.enabled = false;
        }
    }
}

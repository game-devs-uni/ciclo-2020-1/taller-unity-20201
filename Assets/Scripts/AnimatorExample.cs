﻿using UnityEngine;
using UnityEngine.UI;

public class AnimatorExample : MonoBehaviour
{

    [SerializeField] AnimatorButtonList buttonList;
    [SerializeField] Animator loliAnimator;

    void Awake()
    {
        buttonList.buttons[0].onClick.AddListener(SetIdle);
        buttonList.buttons[1].onClick.AddListener(SetRun);
        buttonList.buttons[2].onClick.AddListener(SetJump);
        buttonList.buttons[3].onClick.AddListener(SetDeath);
    }

    void SetRun()
    {
        loliAnimator.SetTrigger("setRun");
    }

    void SetJump()
    {
        loliAnimator.SetTrigger("setJump");
    }

    void SetIdle()
    {
        loliAnimator.SetTrigger("setIdle");
    }

    void SetDeath()
    {
        loliAnimator.SetTrigger("setDeath");
    }

}

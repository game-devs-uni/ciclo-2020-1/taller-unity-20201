﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour
{
    [SerializeField] private float objVelocity = 10.0f;

    // Update is called once per frame
    void Update()
    {
        if (MoveRight())
        {
            //Object moves right
            transform.position = new Vector3(transform.position.x+objVelocity, transform.position.y, transform.position.z);
        }
    }

    bool MoveRight()
    {
        return Input.GetKey(KeyCode.RightArrow);
        //return Input.GetKeyDown(KeyCode.RightArrow);
        //return Input.GetKeyUp(KeyCode.RightArrow);
    }
}

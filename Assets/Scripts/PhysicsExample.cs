﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsExample : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        Debug.Log($"{name} chocó con: {other.gameObject.name}");
    }

    private void OnCollisionExit(Collision other)
    {
        Debug.Log($"{name} dejó de chocar con: {other.gameObject.name}");
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log($"{name} atravesó a: {other.gameObject.name}");
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log($"{name} dejó de atravesar a: {other.gameObject.name}");
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

public class UIExample : MonoBehaviour
{
    [SerializeField] Button button;
    [SerializeField] Slider slider;
    [SerializeField] InputField inputField;
    [SerializeField] Toggle toggle;

    void Start()
    {
        button.onClick.AddListener(() => {
            Debug.Log("Button clicked");
        });

        slider.onValueChanged.AddListener((value) => {
            Debug.Log($"Slider value changed to: {value}");
        });

        inputField.onEndEdit.AddListener((textValue) => {
            Debug.Log($"Input text value is: {textValue}");
        });

        toggle.onValueChanged.AddListener((value) => {
            Debug.Log($"Toggle selected is {value}");
        });
    }

}
